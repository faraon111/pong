var drawTool;
var game, ai, player, ball,gameBall;
var canvas;
var pi = Math.PI;
function RandomInt(min,max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
function Block(color, x, y, width, height) {
    this.color = color;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    // this.r=r;
   // this.vx=vx;
   //  this.vy=vy;
   //  this.bool=bool;
    this.Draw = function()
    {
        drawTool.fillStyle = color;
        drawTool.fillRect(x, y, width, height);
        // drawTool.arc(x,y,r,vx,vy,bool);
    }
}
function Ball(color, x, y, r) {
this.color = color;
this.x = x;
this.y = y;
// this.width = width;
// this.height = height;
this.r=r;
// this.vx=vx;
// this.vy=vy;
// this.bool=bool;
 this.DrawBall = function()
    {
        drawTool.fillStyle = color;
        drawTool.arc(x,y,r,0,pi*2,false);}
}

function Play() {
    var colorTable = "rgba("+RandomInt(0,255)+","+RandomInt(0,255)+","+RandomInt(0,255)+",1";
    var colorAi = "rgba("+RandomInt(0,255)+","+RandomInt(0,255)+","+RandomInt(0,255)+",1";
    var colorPlayer = "rgba("+RandomInt(0,255)+","+RandomInt(0,255)+","+RandomInt(0,255)+",1";
    var colorBall = "rgba("+RandomInt(0,255)+","+RandomInt(0,255)+","+RandomInt(0,255)+",1";
    // объект который задаёт игровое поле
    game = new Block(colorTable, 0, 0, 480, 320);

    ai = new Block(colorAi, 10, game.height / 2 - 40, 20, 80);
    player = new Block(colorPlayer, game.width - 30, game.height / 2 - 40, 20, 80);
    ball = new Ball(colorBall,50,50,5);
    //gameBall = new Ball(colorBall, 150, 100, 75, 0, 2*pi, false);
    // ai.scores = 0;
    // player.scores = 0;

    canvas = document.getElementById("canvasPong");
    canvas.width = game.width;
    canvas.height = game.height;
    drawTool = canvas.getContext("2d");
    DrawConfig();
}
// Отрисовка игры
function DrawConfig() {
    game.Draw(); // рисуем игровое поле
}
Play();
ai.Draw();
player.Draw();
ball.DrawBall();